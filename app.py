#!/usr/bin/env python3
#
# This file is part of bn-local-politics-followers.  bn-local-politics-followers is free software: you can
# redistribute it and/or modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation, version 2.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Copyright 2017, Christoph Görn

"""This is bn-local-politics-followers, Twitter accounts for municipal politics in Bonn, DE
"""

import argparse
import json
import datetime
import time
import locale

import tweepy
import ijson
import pandas as pd
import numpy as np


__license__ = "GPLv3"
__version__ = "0.3.0"

# FIXME refactor repo names into CONSTANTS

locale.setlocale(locale.LC_ALL, "en_US")


def datetime_handler(obj):
    if hasattr(obj, 'isoformat'):
        return obj.isoformat()
    elif isinstance(obj, ...):
        return ...
    else:
        raise TypeError


class StatusRepository():
    def __init__(self):
        self.statuses = []

    def create(self, status):
        self.statuses.append(status._json)

    def persist(self):
        with open('repository.json', 'w') as outfile:
            json.dump(self.statuses, outfile)


def get_tweets(store):
    identity = json.load(open('identity.json'))
    auth = tweepy.OAuthHandler(
        identity["consumer_key"], identity["consumer_secret"])
    auth.set_access_token(
        identity["access_token"], identity["access_token_secret"])
    api = tweepy.API(auth)

    members = [member for member in tweepy.Cursor(
        api.list_members, 'bonndigital', 'lokalpolitik-bonn').items()]
    members.sort(key=lambda x: x.followers_count, reverse=True)

    for member in members:
        print('{},{},{}'.format(member.screen_name,
                                member.followers_count, member.statuses_count))

        for status in tweepy.Cursor(api.user_timeline, screen_name=member.screen_name, trim_user=True).items():
            store.create(status)

        store.persist()


def _flatten_dict(data, layers=1, drop_deeper=True):
    """Flatten a whole dict, optional cut off depth"""
    for _ in range(layers):
        data = [(k, v) if not isinstance(v, dict) else [(k + '_' + k2, v2) for k2, v2 in v.items()] for k, v in
                data.items()]

        data = [item for sublist in data for item in sublist if isinstance(sublist, list)] + [y for y in data if
                                                                                              not isinstance(y,
                                                                                                             list)]
        data = dict(data)

    if drop_deeper:
        data = {k: v for k, v in data.items() if not isinstance(v, dict)
                or isinstance(v, list)}

    return data


def analyse(store_location):
    start_time = time.time()

    def parse_twitter_date(row):
        """convert Twitter's 'Thu Oct 05 18:44:52 +0000 2017' to a datetime"""
        date = datetime.datetime.strptime(
            row["created_at"], "%a %b %d %H:%M:%S %z %Y")

        return date

    tweets = extract_relevant_tweets(store_location)
    tweets['created_at'] = tweets.apply(parse_twitter_date, axis=1)

    days_of_tweets, bin_edges = np.histogram(
        tweets['created_at'].dt.weekday, 7, density=True)

    hours_of_tweets, bin_edges = np.histogram(
        tweets['created_at'].dt.hour, 24, density=True)

    stop_time = time.time()

    tweets_statistics = {
        "total": len(tweets),
        "earliest_created_at": json.dumps(tweets['created_at'].min(), default=datetime_handler),
        "last_created_at": json.dumps(tweets['created_at'].max(), default=datetime_handler),
        "processing_time_seconds": stop_time - start_time,
        "histogram_dow": json.dumps(days_of_tweets.tolist()),
        "histogram_hod": json.dumps(hours_of_tweets.tolist())
    }

    return tweets_statistics


def extract_relevant_tweets(store_location):
    """This one reads all the tweets serialized and stored in the file given as 'store_location'
    """
    good_columns = [
        'created_at',
        'id',
        'text',
        'user_screen_name',
        'retweet_count',
        'favorite_count'
    ]

    df = None

    with open(store_location, 'r') as f:
        tweets_json = json.load(f)

        ds = [_flatten_dict(t, layers=3, drop_deeper=True)
              for t in tweets_json]
        df = pd.DataFrame(ds)

    return df[good_columns]


def split_repository(store_location, small_store_location, n):
    """This will split a repository and store just n % items in the small repository"""
    with open(store_location, 'r') as infile:
        tweets_json = json.load(infile)

    tweets_total = len(tweets_json)
    tweets_small_total = round(tweets_total / 100 / n)

    with open(small_store_location, 'w') as outfile:
        json.dump(tweets_json[0::tweets_small_total], outfile)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Analyse some Twitter accounts and data for municipal politics in Bonn, DE.')
    parser.add_argument("-v",
                        "--verbosity", help="increase output verbosity", action="store_true")

    parser.add_argument("-s",
                        "--small", help="use a small repository", action="store_true")
    command = parser.add_mutually_exclusive_group(required=True)
    command.add_argument("fetch", nargs='?',
                         help="fetch will fetch a lot of tweets")
    command.add_argument(
        "analyse", nargs='?', help="analyse will analyse tweets stored in a repository")
    command.add_argument(
        "split", nargs='?', help="this will split 5% of tweets into a small repository")

    args = parser.parse_args()

    if args.small:
        repository_name = 'repository-small.json'
    else:
        repository_name = 'repository.json'

    if args.fetch == 'fetch':
        store = StatusRepository()

        try:
            get_tweets(store)
        except KeyboardInterrupt as kih:
            store.persist()
    elif args.fetch == 'analyse':  # there is something broken with argparsing FIXME
        print(analyse(repository_name))
    elif args.fetch == 'split':
        split_repository('repository.json', 'repository-small.json', 1)

# https://bl.ocks.org/d3noob/08af723fe615c08f9536f656b55755b4
